import React from 'react'
import styled from 'styled-components'
import MissionSelect from '../components/missionSelect'
import backend from '../config/backend'

import '../style.css'

const Wrapper = styled.div``

const Card = styled.div`
  background-color: #546e7a;
  padding: 15px;
`

const CardContent = styled.div`
  display: flex;
  justify-content: space-around;
`

const StyledTool = styled.div`
  height: 50px;
  width: 50px;
  margin-bottom: 2px;
  background-color: ${({ percentage }) => {
    if (percentage < 20) {
      return '#f44336'
    } else if (percentage < 80) {
      return '#FF9800'
    } else {
      return '#4CAF50'
    }
  }};
  display: flex;
  align-items: center;
  justify-content: center;
`

const ToolWrapper = styled.div`
  text-align: center;
  background-color: #78909c;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
`

const Tool = ({ tool }) => (
  <ToolWrapper>
    <StyledTool percentage={tool.batteryPercentage}>
      {tool.batteryPercentage}%
    </StyledTool>
    <span>{tool.name}</span>
  </ToolWrapper>
)

class Base extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
    fetch(`${backend}/missions/${this.props.url.query.id}`)
      .then(response => response.json())
      .then(mission =>
        this.setState({
          mission,
          steps: mission.steps,
          devices: mission.devices,
        }),
      )
  }

  render() {
    if (!this.props.url.query.id) return <MissionSelect page="base" />
    if (!this.state.mission || !this.state.steps || !this.state.devices)
      return <p>Loading...</p>
    return (
      <Wrapper>
        <h1>Base</h1>
        <Card>
          <h2>Devices</h2>
          <CardContent>
            {this.state.devices.map(e => (
              <Tool />
            ))}
          </CardContent>
        </Card>
      </Wrapper>
    )
  }
}

export default Base
