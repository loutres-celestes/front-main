import React from 'react'
import styled from 'styled-components'
import { ArrowUp } from 'react-feather'
import AppBar from '../components/appBar'

import '../style.css'

const Wrapper = styled.div`
  height: 100vh;
  width: 100%;
  display: flex;
  flex-direction: column;
`

const Chat = styled.div`
  flex-grow: 2;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 15px;
  overflow: scroll;
`

const Message = styled.div`
  background-color: ${({ type }) => (type === 'user' ? '#2196f3' : '#263238')};
  padding: 10px;
  border-radius: 15px;
  margin-bottom: 15px;
  align-self: ${({ type }) => (type === 'user' ? 'flex-end' : 'flex-start')};
`

const TextField = styled.div`
  height: 35px;
  display: flex;
  background-color: #78909c;
  padding-top: 10px;
  flex-shrink: 0;
`

const Button = styled.div`
  height: 30px;
  width: 30px;
  border-radius: 50%;
  background-color: #2196f3;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-left: -15px;
  margin-right: 5px;
  margin-top: -2px;
  cursor: pointer;
`

const Input = styled.input`
  height: 20px;
  flex-grow: 2;
  border-radius: 10px;
  margin-left: 5px;
`

class Chatbot extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      messages: [
        { message: 'Coucou', type: 'user' },
        { message: 'Je suis un bot', type: 'bot' },
      ],
    }
  }

  handleClick = () => {
    if (!this.state.message || this.state.message === '') return
    const newMessages = [
      ...this.state.messages,
      { type: 'user', message: this.state.message },
    ]
    this.setState({
      messages: newMessages,
      message: '',
    })
    setTimeout(() => {
      const newMessages = [
        ...this.state.messages,
        { type: 'bot', message: 'Je suis toujours un bot' },
      ]
      this.setState({
        messages: newMessages,
      })
    }, 1000)
  }

  render() {
    return (
      <Wrapper>
        <AppBar>Chatbot</AppBar>
        <Chat>
          {this.state.messages.map(e => (
            <Message type={e.type}>{e.message}</Message>
          ))}
        </Chat>
        <TextField>
          <Input
            onChange={e => this.setState({ message: e.target.value })}
            value={this.state.message}
            type="text"
          />
          <Button onClick={this.handleClick}>
            <ArrowUp />
          </Button>
        </TextField>
      </Wrapper>
    )
  }
}

export default Chatbot
