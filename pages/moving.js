import React from 'react'
import styled from 'styled-components'
import dynamic from 'next/dynamic'
import Itinerary from '../components/itinerary'
import MissionSelect from '../components/missionSelect'
import backend from '../config/backend'

import '../style.css'

const Map = dynamic(() => import('../components/map'), {
  ssr: false,
})
const Wrapper = styled.div`
  margin: 0;
  background-color: #37474f;
  color: #dddddd;
`

const Details = styled.div`
  text-align: justify;
  padding: 20px;
`

class Moving extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
    fetch(`${backend}/missions/${this.props.url.query.id}`)
      .then(response => response.json())
      .then(mission =>
        this.setState({
          mission,
          steps: mission.steps,
          devices: mission.devices,
        }),
      )
  }
  render() {
    if (!this.props.url.query.id) return <MissionSelect page="moving" />
    if (!this.state.mission || !this.state.steps || !this.state.devices)
      return <p>Loading...</p>
    return (
      <Wrapper>
        <Map />
        <Itinerary />
        <Details>
          <h2>{this.state.mission.name}</h2>
          <div>
            <h3>Steps</h3>
            <ul>
              {this.state.steps.map(e => (
                <li>{e.title}</li>
              ))}
            </ul>
            <h3>Required devices</h3>
            <ul>
              {this.state.devices.map(e => (
                <li>
                  {e.name} : {e.batteryPercentage}%
                </li>
              ))}
            </ul>
          </div>
        </Details>
      </Wrapper>
    )
  }
}

export default Moving
