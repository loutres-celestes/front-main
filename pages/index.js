import React from 'react'
import Link from 'next/link'
import styled from 'styled-components'

import '../style.css'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`

export default () => (
  <Wrapper>
    <Link href="base">Base</Link>
    <Link href="moving">Moving</Link>
    <Link href="mission">Missions</Link>
    <Link href="chatbot">Chatbot</Link>
  </Wrapper>
)
