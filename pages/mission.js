import React from 'react'
import Link from 'next/link'
import styled from 'styled-components'
import { ArrowLeft } from 'react-feather'

import '../style.css'

import CheckItem from '../components/checkItem'
import MissionSelect from '../components/missionSelect'
import AppBar from '../components/appBar'
import backend from '../config/backend'
import { throws } from 'assert'

const Wrapper = styled.div``

const Banner = styled.div`
  width: 100%;
  height: 150px;
  background-image: linear-gradient(${({ gradient }) => gradient});
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 2em;
  border-bottom: 1px solid black;
`

const CheckList = styled.div`
  padding: 10px;
`

const Pinata = styled.img`
  position: fixed;
  right: ${({ pinated }) => (pinated % 2 === 0 ? '-40px' : '100px')};
  bottom: ${({ pinated }) => (pinated % 2 === 0 ? '-90px' : '-20px')};
  overflow: hidden;
  transition: all 0.2s ease;
  cursor: pointer;
`

class Mission extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      checked: [],
      pinated: 0,
    }
  }

  componentDidMount() {
    fetch(`${backend}/missions/${this.props.url.query.id}`)
      .then(response => response.json())
      .then(mission =>
        this.setState({
          mission,
          steps: mission.steps,
        }),
      )
  }

  handleDetails = step => {
    this.setState({
      details: step,
    })
  }

  handleCheck = step => {
    const newChecked = [...this.state.checked, step]
    this.setState({
      checked: newChecked,
    })
  }

  render() {
    if (!this.props.url.query.id) return <MissionSelect page="mission" />
    if (!this.state.mission || !this.state.steps) return <p>Loading...</p>
    return this.state.details ? (
      <div>
        <AppBar>
          <ArrowLeft
            cursor="pointer"
            onClick={() => this.setState({ details: null })}
          />
          {this.state.details.title}
        </AppBar>
        <Banner gradient="135deg, #92FFC0 10%, #002661 100%" />
        <p>{this.state.details.description}</p>
      </div>
    ) : (
      <Wrapper>
        <Banner gradient="135deg, #F97794 10%, #623AA2 100%">
          {this.state.mission.name}
        </Banner>
        <CheckList>
          {this.state.steps &&
            this.state.steps.map(step => (
              <CheckItem
                title={step.title}
                id={step.id}
                checked={this.state.checked.includes(step.id)}
                onCheck={() => this.handleCheck(step.id)}
                onDetails={() => this.handleDetails(step)}
              />
            ))}
        </CheckList>
        {this.state.checked.length === this.state.steps.length &&
          this.state.pinated < 12 && (
            <Pinata
              pinated={this.state.pinated}
              onClick={() => this.setState({ pinated: this.state.pinated + 1 })}
              height="150px"
              src={`static/${
                this.state.pinated > 10 ? 'explosion.png' : 'pinata.png'
              }`}
            />
          )}
      </Wrapper>
    )
  }
}

export default Mission
