import React from 'react'
import styled from 'styled-components'
import ReactMapboxGl, { Layer, Feature } from 'react-mapbox-gl'
import mapboxKey from '../config/mapboxKey'

const Map = ReactMapboxGl({
  accessToken: mapboxKey,
})

export default () => (
  <Map
    style="mapbox://styles/mapbox/streets-v9"
    containerStyle={{
      height: '200px',
      width: '100%',
      zIndex: 1,
    }}
  >
    <Layer type="symbol" id="marker" layout={{ 'icon-image': 'marker-14' }}>
      <Feature coordinates={[-0.481747846041145, 40.3233379650232]} />
    </Layer>
  </Map>
)
