import React from 'react'
import Link from 'next/link'
import styled from 'styled-components'
import backend from '../config/backend'

const Wrapper = styled.div`
  margin-top: 30px;
  display: flex;
  flex-direction: column;
  align-items: center;
`

const Mission = styled.a`
  margin-bottom: 15px;
  text-decoration: none;
  color: white;
  cursor: pointer;
`

class MissionSelect extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
    fetch(`${backend}/missions/`)
      .then(response => response.json())
      .then(missions => this.setState({ missions }))
  }

  render() {
    if (!this.state.missions) return <p>Loading</p>
    return (
      <Wrapper>
        {this.state.missions.map(e => (
          <Mission href={`/${this.props.page}?id=${e.id}`}>{e.name}</Mission>
        ))}
      </Wrapper>
    )
  }
}

export default MissionSelect
