import styled from 'styled-components'

export default styled.div`
  background-color: #607d8b;
  height: 50px;
  font-size: 1.5em;
  display: flex;
  align-items: center;
  padding-left: 10px;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  flex-shrink: 0;
`
