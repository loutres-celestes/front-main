import React from 'react'
import Link from 'next/link'
import styled from 'styled-components'
import { ChevronRight } from 'react-feather'

const StyledCheckItem = styled.div`
  border-bottom: 1px solid #999999;
  padding-top: 10px;
  padding-bottom: 10px;
  display: flex;
  align-items: center;
`

const Label = styled.label`
  text-decoration: ${({ checked }) => (checked ? 'line-through' : 'none')};
  flex-grow: 2;
`

class CheckItem extends React.Component {
  render() {
    return (
      <StyledCheckItem>
        <input
          checked={this.props.checked}
          onChange={this.props.onCheck}
          type="checkbox"
        />
        <Label checked={this.props.checked}>{this.props.title}</Label>
        <ChevronRight onClick={this.props.onDetails} cursor="pointer" />
      </StyledCheckItem>
    )
  }
}

export default CheckItem
