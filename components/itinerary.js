import React from 'react'
import styled from 'styled-components'

const Itinerary = styled.div`
  /* margin-top: -30px; */
  height: 80px;
  width: 100%;
  max-width: 900px;
  margin-left: auto;
  margin-right: auto;
  background-color: #263238;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  border-radius: 30px;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 2;
`

const Bar = styled.div`
  height: 3px;
  width: 90%;
  background-color: #ffffffaa;
  border-radius: 5px;
  display: flex;
`

const Start = styled.div`
  width: 0;
  height: 0;
  border-style: solid;
  border-width: 12.5px 0 12.5px 21.7px;
  margin-right: -20px;
  margin-top: -25px;
  border-color: transparent transparent transparent #4caf50;
`

const End = styled.div`
  width: 0;
  height: 0;
  border-style: solid;
  border-width: 12.5px 21.7px 12.5px 0;
  margin-left: -20px;
  margin-top: -25px;
  border-color: transparent #f44336 transparent transparent;
`

const Progress = styled.div`
  height: 5px;
  width: ${({ percentage }) => `${percentage}%`};
  margin-top: -1px;
  background-color: #ffffff;
  border-radius: 5px;
`

const User = styled.div`
  height: 15px;
  width: 15px;
  margin-top: -7.5px;
  margin-left: -7.5px;
  border-radius: 50%;
  background-color: #2196f3;
  z-index: 1;
`

export default () => (
  <Itinerary>
    <Start />
    <Bar>
      <Progress percentage={50} />
      <User />
    </Bar>
    <End />
  </Itinerary>
)
